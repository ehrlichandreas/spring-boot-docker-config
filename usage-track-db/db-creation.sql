DROP DATABASE IF EXISTS db_usagetrack_name;
CREATE DATABASE db_usagetrack_name;

DROP USER IF EXISTS db_usagetrack_user;
CREATE USER db_usagetrack_user PASSWORD 'db_usagetrack_pass';

GRANT CONNECT ON DATABASE db_usagetrack_name TO db_usagetrack_user;

\connect db_usagetrack_name;
CREATE SCHEMA ppu_data AUTHORIZATION db_usagetrack_user;
